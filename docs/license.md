---
hide:
  - navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# License

Blender itself is released under the [GNU General Public License](https://www.blender.org/about/license/).

Except where otherwise noted, the content of the Blender Developer Documentation is available under a
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/),
or any later version.
Excluded from the CC-BY-SA are logos, trademarks, icons, source code and Python scripts.

Please attribute the “Blender Developer Documentation Team” and include a
hyperlink (online) or URL (in print).
See [recommended practices for attribution](https://wiki.creativecommons.org/wiki/Recommended_practices_for_attribution)
for details.

Example attribution:
> The [Blender Developer Documentation](https://developer.blender.org/docs) by
the [Blender Developer Documentation Team](https://projects.blender.org/blender/blender-developer-docs),
used under [CC-BY-SA v4.0](https://creativecommons.org/licenses/by-sa/4.0/).

This means that after contributing to the documentation you do not hold exclusive
rights to your text. You are, of course, acknowledged and appreciated for
your contribution. However, others can change and improve your text in order to
keep the documentation consistent and up to date.

If you have questions about the license, feel free to contact the Blender
Foundation: foundation (at) blender (dot) org

## Contribution History

The history of contributions and changes can be found at these locations.

* [Developer docs repository](https://projects.blender.org/blender/blender-developer-docs)
* [Archive of 2024 wiki](https://archive.blender.org/wiki/2024/wiki/Main_Page.html)
* [Archive of 2015 wiki](https://archive.blender.org/wiki/2015/)