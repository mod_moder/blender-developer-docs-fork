# Building Blender as a Python Module

## Overview

The official **blender.org** embeds a Python interpreter (CPython 3.x).
This makes sense from the perspective of a user, who's primary needs are
to have an application which can be extended with scripts.

However from the perspective of a Python developer, it can be useful to
bring Blender into your existing scripts and access its feature set.

The option to build Blender as a Python module is not officially
supported, *in the sense Blender.org isn't distributing it along with
regular releases*. Currently, its a build option you can enable, for
your own use.

For details on API usage see: see [Python Reference Manual
Page](https://docs.blender.org/api/3.4/info_advanced_blender_as_bpy.html).

## Building

### Quick Setup

See [building blender](index.md) docs for
instructions, but run `make bpy` instead of `make`.

After building succeeds, there should be a `bin/bpy/` folder with the
module. See the next section for how to install this.

### Manual CMake Setup

Instructions are the same as [manual CMake setup](linux.md#manual-cmake-setup)
for regular builds, but changing the cmake command to include
the configuration for the Python module.

```
cmake -C ../blender/build_files/cmake/config/bpy_module.cmake ../blender
```

See `build_files/cmake/config/bpy_module.cmake` for details about which
options are different than a regular Blender build.

### Older Versions

If using Blender 3.3 or earlier, please refer to
[these build instructions](https://archive.blender.org/wiki/2024/w/index.php?title=Building_Blender/Other/BlenderAsPyModule&oldid=26435.html).

## Installation

### Python Wheel

Blender provides a utility to create a `*.whl` (wheel) file, which
can then be installed.

To create a wheel file, run the following command:

``` sh
python3 ./build_files/utils/make_bpy_wheel.py ../build_linux_bpy/bin/
```

Where the first argument is the directory containing the "bpy"
directory.

The wheel can be installed with pip, e.g:

``` sh
pip3 install bpy-{version-and-platform}.whl
```

### Linux

#### System Wide Install

You may want to copy into the module to the systems Python path, e.g.:

```
/usr/lib/python3.10/site-packages
```

For a system wide installation:

```
WITH_INSTALL_PORTABLE=OFF
```

> NOTE: `PYTHON_SITE_PACKAGES` will be used as the target path, but
this is auto detected, nevertheless, you may want to modify.

Once these options are set, from the BPY build directory
(`build_linux_bpy/` by default) run:

``` sh
make install
```

#### Local Install

Alternately you might want to use your user Python path (see
<https://www.python.org/dev/peps/pep-0370/>)

```
$HOME/.local/lib/python3.10/site-packages
```

For a local install use the following options:

```
WITH_INSTALL_PORTABLE=ON
CMAKE_INSTALL_PREFIX=$HOME/.local/lib/python3.10/site-packages
```

Once these options are set, from the BPY build directory
(`build_linux_bpy/` by default) run:

```
make install
```

### Windows

Copy the bpy directory into Python's site-packages:

``` sh
xcopy /E bin\bpy C:\Python310\Lib\site-packages\bpy
```

### macOS

#### Local Install

``` sh
mkdir -p $HOME/.local/lib/python3.10/site-packages/
cp -r ./bin/bpy $HOME/.local/lib/python3.10/site-packages/
```

#### System Wide Install

For a Python installation from python.org:

``` sh
cp -r ./bin/bpy /Library/Frameworks/Python.framework/Versions/3.10/lib/python3.10/site-packages/
```

## Testing

This allows 'bpy' to be imported from Python or other applications/IDE's
which embed python, eg:

``` sh
python -c "import bpy, os; bpy.ops.wm.save_as_mainfile(filepath=os.path.abspath('my.blend'))"
```

On Linux, using the GPU to render is supported (for EEVEE rendering):

``` sh
python -c "import bpy; bpy.ops.render.render(write_still=True)"
```

This runs in background mode and has similar restrictions to running a
script:

``` sh
blender --background --python test.py
```

If the build folder is still there, `ctest -VV -C config` (where
config is Debug or Release etc.) can be run in the build folder to see
if bpy has been installed properly. [CTest
docs](https://cmake.org/cmake/help/latest/manual/ctest.1.html).

## Troubleshooting

- The Python version requirements are the same with building a regular
  blender binary (if Blender is using Python3.10 then there is NO WAY to
  use another version - 2.7..3.5/3.6 will all fail).
- On Windows, you probably won't want to use a debug build, since this
  requires a debug python installation (python37_d.dll rather then
  python37.dll), so while it can be made to work, its more trouble.
