# Setup

Tests require Blender to be built, see the [official build
instructions](../building_blender/index.md). This document assumes the
directory layout to be:

``` text
~/blender-git/blender  # Blender source directory
~/blender-git/build    # Blender build directory
```

Running the tests is then done with `make test`:

``` bash
cd ~/blender-git/blender
make test
```

[GTest](gtest.md) tests need Blender to be built
with `WITH_GTESTS` enabled.

## Downloading Test Files

Additional binary test files are needed for running the Blender tests.
These are available through an SVN repository, separate from the Blender
source repository.

`make test` will automatically download these files the first time it
runs.

To download the test files manually:

``` bash
cd ~/blender-git
mkdir lib
cd lib
svn checkout https://svn.blender.org/svnroot/bf-blender/trunk/lib/tests
```

## Updating Test Files

Test files change along with the Blender source code. `make update`
will update tests to the latest version if you have downloaded them
once, together with the Blender source code.

``` bash
cd ~/blender-git/blender
make update
```

## Running Tests

`make test` runs all available tests. For fine control, use
[ctest](https://cmake.org/cmake/help/latest/manual/ctest.1.html)
directly.

`ctest` must be run from the build directory.

``` bash
cd ~/blender-git/build
```

Some example commands are:

``` bash
# List available tests
ctest -N
# Run all tests with name matching "alembic"
ctest -R alembic
# Show verbose output for a test, to get more information on why it failed
ctest -VV -R alembic
# Run tests in parallel, and show verbose output for failed tests
ctest -j 4 --output-on-failure
```

When building with Visual Studio or Xcode, the build configuration needs
to be specified for every `ctest` command, for example:

``` bash
ctest -C Release -R alembic
```

Some GTest tests bundle together all tests within a Blender module. It's
possible to run only specific test within a module as follows. Find the
name of the module test, for example by listing all tests with `ctest
-N`.

      ...
      Test  #5: bf_blenkernel_tests
      ...

Then run the test with verbose output:

``` bash
ctest -R bf_blenkernel_tests -V
```

This shows the exact command that is run for each test. You can then
copy-paste that command to run it yourself.

### ASAN builds

Running tests on ASAN builds is tricky, because ASAN will fail most of
them due to false/unrelated memory leak detection. However, completely
'silencing' ASAN reports using the `exitcode=0` `ASAN_OPTIONS` is a
(very) bad idea, as it will also hide many actual issues, including
segfaults!

The first step is to remove memleaks errors which happen in third party
libraries, see [the ASAN
page](../tooling/asan.md#quiet-leaksanitizer)
for details.

The `guardedalloc` test intentionally attempts to allocate an invalid
amount of memory and expects to fail and return NULL in that case.
`allocator_may_return_null=true` is required for this test to behave
as expected.

``` bash
ASAN_OPTIONS="allocator_may_return_null=true" LSAN_OPTIONS="print_suppressions=false:suppressions=/path/to/blender/tools/config/analysis/lsan.supp" ctest -j 4 --output-on-failure
```

> NOTE: **No LSAN Suppressions**
> In case LSAN suppressions do not work for some reason, it is also
possible to pass leak_check_at_exit=false to ASAN_OPTIONS. However,
doing this will also hide valid memleaks reports, which should be
avoided as much as possible.

## Adding Tests

Tests can be written in [Python](python.md) or
[C++](gtest.md). For tests that can easily be
written in Python, this is preferred. Lower-level tests can be written
in C++ files next to the sources under test. See the language-specific
pages for more info.
