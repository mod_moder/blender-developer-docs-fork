# BuildBot

[BuildBot](https://builder.blender.org) automatically builds and tests
Blender and its documentation on all supported platforms.

This page provides an overview of how to use the BuildBot for Blender
developers.

## Daily Builds

Buildbot automatically builds the **master** branch and any **release**
branch under active development.

- Nightly builds are packaged and made available on [download page](https://builder.blender.org/).
  - Builds are code signed for Windows and macOS.
- On individual commits: only compiles and runs the tests.
  - A range of commits is collected once the branch is stable **for at
    least 15 minutes**

## Pull Request Builds

Pull requests on [projects.blender.org](https://projects.blender.org)
can be built using [blender-bot commands](https://projects.blender.org/infrastructure/blender-bot/src/branch/main/README.md),
to test for build issues or to make a package available for download. It
is highly recommended to test any non-trivial changes on the buildbot
before merging.

Pull request must generally must be up to date with the latest `main`
branch to build successfully. For security reasons, only developers with
commit access can start such builds.

## Branch Builds

Branches can also be built using [blender-bot commands](https://projects.blender.org/infrastructure/blender-bot/src/branch/main/README.md).
For this put the comment in an issue relevant to the branch.

Branch builds remain available for 30 days, or until a newer revision of
the branch is built.

## Manual Triggers

Only a few developers have accounts on build.blender.org to trigger and
cancel builds there. It is recommended to use blender-bot instead when
possible. Anonymous users cannot start any builds, this is restricted
for security reasons.

Developers with an account can click on the "Anonymous" button in the
top right to log in.

### Branch Builds

To build a branch:

- Go the [builders page](https://builder.blender.org/admin/#/builders)
  and click the **vexp-code-experimental-coordinator** builder.
- Click **Trigger Experimental Build**.
- Fill in the name of the branch in the Blender repository.
- If the build must be made available for download, enable **Package
  delivery**.
- Click **Start Build**.
- Follow progress of the submitted jobs, logs can be inspected for each
  platform.
- Successful builds will appear on
  [builder.blender.org/download/experimental](https://builder.blender.org/download/experimental/)
  and
  [builder.blender.org/download/experimental/branchname/](https://builder.blender.org/download/experimental/branchname/)

There is a mechanism to automatically build and deliver specific
branches every night, for specific temporary projects like `cycles-x`
and `studio-spright-fright`.

### Pull Request Builds

To build a pull requests:

- Go the [builders page](https://builder.blender.org/admin/#/builders)
  and click the **vexp-code-patch-coordinator** builder.
- Click **Trigger Patch Build**.
- Fill in the pull request ID (e.g. \#1234).
- If the build must be made available for download, enable **Needs
  package delivery**.
- Click **Start Build**.
- Follow progress of the submitted jobs, logs can be inspected for each
  platform.
- Successful builds will appear on
  [builder.blender.org/download/patch](https://builder.blender.org/download/patch/).

Eventually, the plan is to support automatic builds for code review.

## Release

Builds from the buildbot are used for official releases.

Once the release cycle in `BKE_blender_version.h` is set to
`release` (or `rc` for release candidates), all the necessary
packages, installers and hashes will be generated.

## Store Delivery

There are automatic builders for delivering builds to various app
stores.

- Steam
  - Builds are automatically uploaded every night
  - `daily` (master), `daily-3.3` and `daily-2.93` Steam branches
    are automatically associated with the latest builds
  - `default`, `3.3`, `2.93` and similar branches are manually
    associated with automatically uploaded builds in Steamworks on
    release
- Snap:
  - Builds are automatically uploaded every night
  - `latest/edge`, `3.x/candidate` or `3.x/beta`,
    `3.3lts/candidate` and `2.93lts/candidate` tracks are
    automatically associated with the latest builds
  - `XXX/stable` tracks are manually associated with automatically
    uploaded builds in the Snapcraft store on release
- Windows Store:
  - Builds are not automatically uploaded, however a `.msix` package
    is made every night

## Build Configurations

- `release` is used for packaged builds, both daily and patch builds.
- `asserts` is used for non-package builds, both daily and patch
  builds. The only difference with `release` is that it enables
  asserts to catch bugs in tests.
- `sanitizer` is an experimental option to use address sanitizer for
  patch builds. The goal is for this to replace `asserts` to catch
  more bugs in tests, once it is passing tests and working reliably.
- `debug` is an option to make a debug build for patches.

## Builds Listing API

A simple JSON API to list all available builds is accessible at

    https://builder.blender.org/download/<category>/?format=json&v=1

For example, querying
<https://builder.blender.org/download/daily/?format=json&v=1> will
return the daily builds.

The format of the response is a list of objects like this:

        {
            "url": "https://builder.blender.org/.../full-url-to-archive",
            "app": "Blender",
            "version": "3.3.11",
            "risk_id": "candidate",
            "branch": "v33",
            "patch": null,
            "hash": "ae9665bc449f",
            "platform": "linux",
            "architecture": "x86_64",
            "bitness": 64,
            "file_mtime": 1692882641,
            "file_name": "blender-3.3.11-candidate+v33.ae9665bc449f-linux.x86_64-release.tar.xz",
            "file_size": 197542184,
            "file_extension": "xz",
            "release_cycle": "candidate"
        }

## File Naming Convention

When automatically downloading builds, please use the JSON API. Below is the
current file naming convention, but this may change in the future.

**Format description:**

```
<app_id>-<version_id>-<risk_id>+<branch_id>.<commit_hash>-<platform>.<architecture>-<build_configuration>.<file_extension>
```

**Example file name:**

```
blender-3.0.0-alpha+master.ba698f081270-darwin.x86_64-release.dmg
```

**Example regular expression for parsing web page download:**

```
$regexp = '^(?<app_id>(blender))\-' + `
	'(?<version_id>[0-9]+\.[0-9]+\.[0-9]+)\-' + `
	'(?<risk_id>[a-z]+)\+' + `
	'(?<branch_id>[A-Za-z0-9_\-]+)\.' + `
	'(?<commit_hash>[a-fA-F0-9]+)\-' + `
	'(?<platform>[A-Za-z0-9_]+)\.' + `
	'(?<architecture>[A-Za-z0-9_]+)\-' + `
	'(?<build_configuration>(release|debug))\.' + `
	'(?<file_extension>[A-Za-z0-9\.]+)'
```

The element `<branch_id>` is trimmed of redundant information such as
`blender-` and `-release` words from the branch name of all release branches.

## Archived Builds

Archived builds can be accessed by selecting the **All Archived Builds** link.

These are available for a maximum of 90 days, then they are purged from the
system.

## Glossary

| Name        | Definition                                                                                                                                                                                                                                                                       | Notes                                                                     |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------|
| Coordinator | A coordinator is a builder that allows users to input choices to create multiple builder requests.                                                                                                                                                                               | Only coordinators can send build requests to other builders.              |
| Builder     | A [builder](https://docs.buildbot.net/current/manual/concepts.html?highlight=scheduler#builders-and-build-factories) is responsible for creating new builds from [build requests](https://docs.buildbot.net/current/manual/concepts.html?highlight=scheduler#buildrequests).     | Normal builders may not start new build requests. See coordinators above. |
| Worker      | A [worker](https://docs.buildbot.net/current/manual/concepts.html?highlight=scheduler#concepts-worker) corresponds to an environment where builds are executed.                                                                                                                  |                                                                           |
| Scheduler   | A [scheduler](https://docs.buildbot.net/current/manual/concepts.html?highlight=scheduler#schedulers) is a component that decides when to start a build. The decision could be based on time, on new code being committed or on similar events. They always trigger coordinators. |                                                                           |
