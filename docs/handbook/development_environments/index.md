# Development Environments

## C/C++

If you already are familiar with a C/C++ development environment, most
likely you can use it for Blender development. This document just a
few commonly used by Blender developers and known to work well.

Whichever environment you choose, make sure you *also* set up
[Clang Format](../tooling/clangformat.md) for your environment.
This is the required method for code formatting.

If you have no strong preference and are on Windows or macOS, it's
safe to choose respectively Visual Studio and Xcode. These are
easiest to set up as part of the build process.

Visual Studio Code is popular as an IDE that works across all platforms.

- [Xcode](../building_blender/mac.md#build-as-an-xcode-project) on macOS
- [Visual Studio](../building_blender/windows.md#building-from-within-the-visual-studio-ide) on Windows
- [Visual Studio Code](vscode.md) on all platforms
- [QtCreator](qtcreator.md) on Linux

## Python

While python scripts can be developed entirely inside Blender, the
built-in text editor is limited. It's convenient for short scripts or
making minor edits but for large projects you will want to use an
external editor.

With python development the choice of IDE/Editor is less of an issue as
with C/C++, most developers just use their favorite editor.

Editors known to work well: scite, geany, notepad++, gedit and eric5.

If you're already familiar with vim or emacs they are fine too.

For more information on how to execute scripts externally see:
[Using an External Editor](https://docs.blender.org/api/current/info_tips_and_tricks.html#use-an-external-editor)
