# Projects

Projects gather multiple developers and contributors to work on specific
topics. They have a start and end date, while module work is always ongoing.

The main purpose of projects is to allocate time from developers funded
by the Blender Foundation to do focused work on important topics. Other
developers and contributors are welcome to be involved too.

Not all major Blender development needs to be organized as projects, it
can be tackled within modules as well. But projects help clarify the targets
for developers funded by the Blender Foundation.

## Roles

The roles of owners and members are the same as in [modules](modules.md).

## Starting a Project

Admins and the Blender Foundation chairman review and approve proposals
for projects and fit them in the overall planning. Both a design and
engineering plan should be presented.

Projects are announced in articles on the [code blog](https://code.blender.org).

## Running a Project

Once started, a project owner is accountable for the project organization
and outcome. If the project owner is not an admin, one go-to admin will be
consulted about the project progress.

The project page should be as simple and to the point as possible.
Design and engineer plans can have dedicated tasks and documentation,
however a single-paragraph or bullet point version of it is important
to keep the main project task compact.

## Completion

After the project completes the outcome becomes part of regular module
maintenance.

## Ongoing Projects

- [EEVEE Next](https://projects.blender.org/blender/blender/projects/5)
  ([blog](https://code.blender.org/2021/06/eevees-future/))
- [GPU Compositer](https://projects.blender.org/blender/blender/issues/106533)
  ([blog](https://code.blender.org/2022/07/real-time-compositor/))
- [Brush Assets](https://projects.blender.org/blender/blender/projects/30)
  ([blog](https://code.blender.org/2022/11/brush-and-draft-assets/))
- [Grease Pencil v3](https://projects.blender.org/blender/blender/projects/6)
  ([blog](https://code.blender.org/2023/05/the-next-big-step-grease-pencil-3-0/))
- [Extensions](https://projects.blender.org/blender/blender/issues/117286)
  ([blog](https://code.blender.org/2022/10/blender-extensions-platform/))
- [Character Animation](https://projects.blender.org/blender/blender/projects/1)
  ([blog](https://code.blender.org/2022/11/the-future-of-character-animation-rigging/))