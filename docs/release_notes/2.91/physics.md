## Rigid Bodies

- Support for "Compound Shape" collisions
  (blender/blender@820ca419e098).

<figure>
<video src="../../../videos/Chains_comp.mp4" title="Simulation in real time at 60fps" width="500" controls=""></video>
<figcaption>Simulation in real time at 60fps</figcaption>
</figure>

- You now can combine multiple primitive shapes into a concave shape.
  For instance, to a chain link collision shape you can use multiple
  wire edge child objects:

![](../../images/Chain_link_compound_shapes.png){style="width:500px;"}

- Doing it like this makes the simulation run a lot faster than if using
  the "mesh" collision shape. It is also in most cases a lot more
  stable.

## Fluid

- Improved obstacle handling:
  - Advanced obstacle-liquid interaction (UI toggle "Fractional
    Obstacles") has been improved
  - Particles now flow more smoothly over inclined obstacles
  - For even finer control, there is a new option "Obstacle Distance"
    which defines the distance that liquid will maintain towards any
    obstacle.

| Old                                                                                                                                                    | New                                                                                                                                                 |
| ------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| <video src="../../../videos/Beach_2901.mp4" title="Old simulation behavior (2.90): Some particles stick to obstacles" width="500" controls=""></video> | <video src="../../../videos/Beach_291.mp4" title="New simulation behavior (2.91): More fluid flow along obstacles" width="500" controls=""></video> |

- OpenVDB cache:
  - In `.vdb` format, liquid simulations can now be saved with *Full*
    (32 bit), *Half* (16 bit) or *Mini* (mix of 8 bit and 16 bit)
    precision
    (blender/blender@2ec07dfa182d,
    blender/blender@3d1b5e35bddb)
  - Especially scenes with large amounts of liquid particles benefit
    from smaller cache file sizes as viewport performance increases
    significantly
  - E.g. beach scene from above (~300.000 liquid particles): Before
    **4.1 GB**, now (*Mini* format) **1.26 GB**

<!-- -->

- Improved "Viewport Display" options:
  - *Note: This feature is aimed at developers and when a deeper
    inspection of the simulation is required.*
  - It is now possible to visualize liquid simulations grids (before
    only smoke).
  - Vector display options have been improved: Velocities and forces can
    now be viewed per axis (MAC visualization).
  - An option to display gridlines for the underlying simulation grid
    has been added.
  - Cell coloring options have been added: Obstacle, fluid and empty
    cells can be identified in the underlying grid.
  - In-depth report: [Liquid Simulation Display Options](https://archive.blender.org/wiki/2024/wiki/User:Sriharsha/GSoC2020/Final_Report.html)
    (blender/blender@f137022f9919)

![Visualization of a 2D slice of the fluid level-set representation in a liquid simulation](../../images/Fluid_Levelset_Representation.png)

![MAC Grid visualization for a 2D slice of the velocity vector field in a liquid simulation.](../../images/Final_Fluid_MAC_Vectors.png)
