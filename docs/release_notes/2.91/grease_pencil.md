# Blender 2.91: Grease Pencil

## User Interface

- Renamed Boundary Fill `Default` to `All`.
  (blender/blender@fff12be9456c)
- Boundary strokes are now visible, except in render mode. This was
  removed by error in the refactor.
  (blender/blender@ebf5ff8afb9a)
- Brush Advanced panel now has `Ignore Transparent` and `Threshold`
  parameters related.
  (blender/blender@9e644b98a6f7)
- Set Onion Keyframe mode to `All` by default. Also renamed option
  from `All Types` to `All`.
  (blender/blender@3e56dd8fd921)
- Replace Interpolate shortcut to `Ctrl+E` instead of `Ctrl+Alt+E`.
  (blender/blender@ee49ce482a79)
- New Subdivision parameter for Primitives in Topbar.
  (blender/blender@90baead9792b)

## Operators

- New Cleanup Frames operator.
  (blender/blender@382b9007f8f3)
- New Step parameter for Interpolate Sequence.
  (blender/blender@97871e16ff45)
- The convert mesh to strokes and bake mesh animation, now create layers
  and materials using the original object name. Also fixed problems when
  convert or bake animation for several objects at the same time.
  (blender/blender@256b59b76f0a)
- New material option to set as Holdout. This allows to open holes in
  filled areas. This option only works when the stroke uses Material
  mode.
  (blender/blender@0335c1fd21dd)

<figure>
<video src="../../../videos/Gpencil_holdout.mp4" title="Holdout Material" width="600" controls=""></video>
<figcaption>Holdout Material</figcaption>
</figure>

- New Trace Images operator using Potrace library.
  (blender/blender@4d62bb8fe57c)

<figure>
<video src="../../../videos/TraceGP.mp4" title="Trace Image" width="600" controls=""></video>
<figcaption>Trace Image</figcaption>
</figure>

This functionality is designed to trace a black and white image into
grease pencil strokes. If the image is not B/W, the trace try to convert
to bitone image. For better results, use manually converted to black and
white images and try to keep the resolution of the image small. High
resolutions could produce very dense strokes.

## Tools

- Draw Brushes: New predefined mode.
  (blender/blender@7becd283cc27)
- Fill Brush: New Layer modes.
  (blender/blender@8fc7c3539ade)
- Fill Brush: Fill inverted pressing `Ctrl` key. Also added new
  direction buttons for Fill in topbar.
  (blender/blender@4ada2909566c)

## Modifiers and VFX

- Offset modifier scale thickness when modify scale.
  (blender/blender@a0b0a47d8104)
