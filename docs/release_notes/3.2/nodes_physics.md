# Geometry Nodes

### Performance Improvements

- Many curve nodes have been ported to a new data-block, which
  significantly improves performance with many curves.
  - Updating the *Endpoint Selection* node resulted in an observed 13x
    performance improvement combined with the *Set Position* node
    (blender/blender@471f27d66bd7).
  - The *Mesh to Curve* node has improved performance, with 10x
    improvements possible when many curves are created
    (blender/blender@901a03725ed581).
  - The *Curve to Mesh* node can be 4-5x faster for inputs with millions
    of curves
    (blender/blender@7484f274dcdc90).
  - The performance of the *Resample Curves* improved by roughly 6x when
    sampling many small poly curves
    (blender/blender@60a6fbf5b59911).
  - Updating the *Realize Instances* improves its performance by 2-3x,
    especially with many small curves
    (blender/blender@6c6f591f909a02).
  - A complete list of the updated nodes can be found
    [here](https://developer.blender.org/T95443).
- Many function nodes with more inputs like *Map Range*, *Combine XYZ*,
  and *Random Value* are 2-4x faster in common cases
  (blender/blender@ae94e36cfb2f3b).
- Selection inputs are now faster in general as the field system can
  prepare the data for nodes more efficiently
  (blender/blender@d3a1e9cbb92cca).
  - In a simple test with the set position node, the improvement to
    runtime was 1.4x with 10% of points selected, and 2.5x with 90% of
    points selected.
- Simple functions with a single input and output in a field network
  like data-type conversion can be around twice as fast in some cases
  (blender/blender@67c42e7f034aad).
- Writing to vertex groups inside nodes and directly with a modifier
  output attribute can be 3.6x faster
  (blender/blender@610ec34c1f7dbd).
  - Values of zero are not written to the group anymore.
- The *Mesh Line* primitive was parallelized with an observed 4x
  performance improvement
  (blender/blender@8f344b530a6ed8).
- A small optimization to the *UV Sphere* node made it 1% faster
  (blender/blender@156e07232e79d5).

### General

- The **Duplicate Elements** can duplicate points, edges, faces,
  instances, and curves
  (blender/blender@120f16fa1f1e).
  - Updating the node to use the new curves data-block improved
    performance by up to 150x
    (blender/blender@663bd38ed65eb0).
- Geometry nodes has better support for byte colors, to work better with
  the changes in [sculpt
  mode](sculpt.md)
  (blender/blender@b9799dfb8a78dd).
  - The *Store Named Attribute* node can write byte colors directly.
  - The "Convert Attribute" operator can convert to/from byte colors.
  - Various systems like implicit conversions and mixing byte colors
    work in all nodes.
  - Byte colors are displayed as integers (0-255) in the spreadsheet.
- Adding a new Geometry Nodes modifier from the modifier panel doesn't
  automatically populate it with a new node tree anymore
  (blender/blender@08b4b657b64f).
- It's now possible to specify a default name for modifier attribute
  inputs and outputs, used whenever creating assigning a node group to
  an empty modifier
  (blender/blender@0ad73bb9655f04).

### Named Attributes

![New nodes to deal with named attributes directly in 3.2](../../images/Named_Attribute_Nodes_3.2.png){style="width:600px;"}

- Nodes to deal with attribute names directly are back!
  (blender/blender@d4e46c13cc9,
  blender/blender@3e7ee3f3bcd61a)
  - The **Store Named Attribute** node can create attributes with any
    name (even names computed with string nodes).
  - The **Remove Named Attribute** node removes any attribute, which can
    be helpful as a performance improvement.
  - The **Named Attribute** input node retrieves an attribute with a
    name for use in a field.
    - The input node supports attribute search, which automatically sets
      its output type
      (blender/blender@3ecdfd09386ee7).
- Attribute names used in node groups are visualized externally.
  - A new "Internal Dependencies" modifier panel shows all attribute
    names used by the modifier
    (blender/blender@0178e694b72d32,
    blender/blender@9a53599180041c).
  - A node editor overlay shows attribute names used by names and node
    groups
    (blender/blender@c71013082d0969).

### Meshes

- The **Face is Planar** nodes can tell when the triangles of an N-Gon
  don't lie on the same plane
  (blender/blender@75bb99fa40dd).
- The *Merge by Distance* node now has a "Connected" mode like the weld
  modifier, which can be much faster
  (blender/blender@81ec3dce6542f9).

### Curves

- The behavior when writing directly to the `position`,
  `handle_right`, and `handle_left` attributes has been simplified,
  removing the edit-mode-like logic affecting handles
  (blender/blender@4c66bd5da25677,
  blender/blender@f8fe0e831ec14c).
  - The *Set Position* and *Set Handle Position* nodes still have the
    more complex behavior.

### Legacy Geometry Nodes

- Most nodes from 2.93, before the
  [fields](https://docs.blender.org/manual/en/latest/modeling/geometry_nodes/fields.html)
  system was introduced, have been removed
  (blender/blender@943b919fe807b5,
  blender/blender@213d00607ea00d).
- This means that as of 3.2, geometry nodes files are *not backwards
  compatible* with files using legacy nodes. Versions 3.1 and 3.0 can be
  used to port files to the new system.
- This change can speed up development and remove the maintenance burden
  of supporting the set of legacy nodes.

# Node Editor

- The readability of node link selections has been improved
  (blender/blender@75bb99fa40dd).
  - The entire link is highlighted when a connected node is selected.
  - Selected links are displayed on top, so they are always clearly
    visible.
  - Selected links are not faded when the sockets they are connected to
    are out out view.
  - Dragged links still get a partial highlight when they are only
    attached to one socket.
- The look of the soft shadow behind nodes has been improved for extreme
  zoom levels and high resolution displays
  (blender/blender@ed4b03215533e6).
- The dot grid background has been improved to be more visually
  consistent with the zoom level
  (blender/blender@26700320389c).
- There have been small improvements to the look of the reroute node
  (blender/blender@ee1d5fb9e467).
  - Curved node links always attach horizontally to the reroute node to
    avoid breaking the visual flow.
  - The label of the reroute node is now center aligned.
- When creating node groups, hidden links are removed, to avoid making
  unused input and output sockets
  (blender/blender@4edde244da3371).
