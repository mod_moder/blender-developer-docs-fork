# Add-ons

## Rigify

- Adjusted the switchable parent rig so that disabling Local Location on
  IK controls aligns location channels to world as intended. Added an IK
  Local Location option to limbs (on by default for backward
  compatibility) and disabled it in metarigs.
  (blender/blender-addons@7120e9c9e087,
  blender/blender-addons@c268b58c247)

## FBX I/O

- Support for camera DoF focus distance was added, including animation,
  for both import
  (blender/blender-addons@5eff3aa881bf)
  and export
  (blender/blender-addons@fba4f07bc695).
- Imported animations of rigs now have their FCurves properly grouped by
  bones
  (blender/blender-addons@a2c3cfdd764b).
- An option to triangulate geometry has been added to the exporter
  (blender/blender-addons@d700a6888ecd).
- An option to only export visible objects has been added to the
  exporter
  (blender/blender-addons@4075cdbdc751).

## BHV I/O

- Imported animations of rigs now have their FCurves properly grouped by
  bones
  (blender/blender-addons@a2c3cfdd764b).

## glTF 2.0 I/O

### Exporter

- Huge refactoring (better hierarchy / collection / instances
  management)
  (blender/blender-addons@782f8585f4cc,
  blender/blender-addons@1d5c8b54ee99,
  blender/blender-addons@64f462358522,
  blender/blender-addons@dbb9b62991b3)
- Use rest pose for armature when 'use current frame' is off
  (blender/blender-addons@d82581163628)
- Add glTF settings node in shader menu
  (blender/blender-addons@5838e260366a)
- Export armature without skined mesh as skin in glTF files
  (blender/blender-addons@5a557e72fc1e)
- Normalize skin weights also for all influence mode
  (blender/blender-addons@96eb14698456)
- Manage active UVMap correclty, when there is no UVMap used in node
  tree
  (blender/blender-addons@3a299965833c)
- Manage skinning when some vertices are not weights at all
  (blender/blender-addons@564fdcdf7159)
- Armature exports all actions
  (blender/blender-addons@9818afc829a0,
  blender/blender-addons@787ea78f7fa6,
  blender/blender-addons@a0ce684afe3e)
- Add various hooks
  (blender/blender-addons@43477e00ce44,
  blender/blender-addons@8653db380888,
  blender/blender-addons@fef8f1ffdb1f)
- Remove back compatibility of export_selection -\> use_selection
  (blender/blender-addons@4d8b2dc95f98)
- Weight min threshold for skinning
  (blender/blender-addons@c157125ace85)
- Option to optimize animation size off by default
  (blender/blender-addons@22c1eb5c3d50)
- Manage use_nla option to avoid exporting merged animations
  (blender/blender-addons@6e430d231788)
- Performance : better way to detect weird images
  (blender/blender-addons@cbb4b8c121a5)
- Fix PBR values when emission node is used
  (blender/blender-addons@fbb5111e1551)
- Fix transmission export
  (blender/blender-addons@4c1df0f54ce0)
- Fix bone export when Yup is off
  (blender/blender-addons@dfe74f0a1b1b)
- Fix active scene export + option to use only active scene
  (blender/blender-addons@1bffd880b8f0)
- No texture export when original can't be retrieved
  (blender/blender-addons@adf10c631570)
- Use color attribute API instead of vertex color
  (blender/blender-addons@485b4ac5691c)

### Importer

- Fix when glTF file has no scene
  (blender/blender-addons@b294d4c76654)
- More animation hooks for user extensions
  (blender/blender-addons@2183b1d29571)

## Dynamic Brush Menus

- Fix and consistently support symmetry menus for all painting modes
  (blender/blender-addons@c9fba919c2e1).
- Fix brushes menu not displaying fully in sculpt mode
  (blender/blender-addons@4d53ec76a3d8).

## Collection Manager

- An option to enable/disable the QCD 3D Viewport header widget has been
  added to the preferences
  (blender/blender-addons@a65df677f707).

## Node Wrangler

- Node Wrangler now handles attributes
  (blender/blender-addons@dde5915336d503e12b43af6611c6947767d3e355).
