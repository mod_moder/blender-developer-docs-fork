# Blender 4.0: Sculpt, Paint, Texture

## Enhancements

- Vertex Paint: Add set vertex colors option to lock alpha (blender/blender!111002)
- Sculpt: Remove dynamic topology "Smooth Shading" option (blender/blender!110548)
- Sculpt: Expand Mask inverted fill (blender/blender!111665)
- Weight Paint: Allow setting weights (Ctrl+X) without paintmask enabled
  (blender/blender@0b1d2d63fe)
