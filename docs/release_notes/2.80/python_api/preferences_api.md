# Blender 2.80: Preferences API

What was formally named *User Preferences*, is now just *Preferences*.
The Python API was tweaked accordingly:

| Old                            | New                       |
|--------------------------------|---------------------------|
| `bpy.context.user_preferences` | `bpy.context.preferences` |

The Python API was also updated to be (mostly) synced with changes in
the placement of options within the UI. The following members of
`bpy.context.preferences` were changed:

| Old                                 | New                                  |
|-------------------------------------|--------------------------------------|
| `system.use_weight_color_range`     | `view.use_weight_color_range`        |
| `system.weight_color_range`         | `view.weight_color_range`            |
| `system.color_picker_type`          | `view.color_picker_type`             |
| `view.use_quit_dialog`              | `view.use_save_prompt`               |
| `view.use_mouse_depth_navigate`     | `input.use_mouse_depth_navigate`     |
| `view.use_mouse_depth_cursor`       | `input.use_mouse_depth_cursor`       |
| `view.use_cursor_lock_adjust`       | `input.use_cursor_lock_adjust`       |
| `view.use_camera_lock_parent`       | `input.use_camera_lock_parent`       |
| `view.use_zoom_to_mouse`            | `input.use_zoom_to_mouse`            |
| `view.use_auto_perspective`         | `input.use_auto_perspective`         |
| `view.use_rotate_around_active`     | `input.use_rotate_around_active`     |
| `system.use_text_antialiasing`      | `view.use_text_antialiasing`         |
| `system.text_hinting`               | `view.text_hinting`                  |
| `system.font_path_ui`               | `view.font_path_ui`                  |
| `system.font_path_ui_mono`          | `view.font_path_ui_mono`             |
| `system.use_international_fonts`    | `view.use_international_fonts`       |
| `system.language`                   | `view.language`                      |
| `system.use_translate_tooltips`     | `view.use_translate_tooltips`        |
| `system.use_translate_interface`    | `view.use_translate_interface`       |
| `system.use_translate_new_dataname` | `view.use_translate_new_dataname`    |
| `input.show_ui_keyconfig`           | `keymap.show_ui_keyconfig`           |
| `input.active_keyconfig`            | `keymap.active_keyconfig`            |
| `system.author`                     | `filepaths.author`                   |
| `system.use_scripts_auto_execute`   | `filepaths.use_scripts_auto_execute` |
| `system.use_tabs_as_spaces`         | `filepaths.use_tabs_as_spaces`       |

Note that even though the undo settings were moved to a different
section too, decision was to keep their Python paths untouched. Reason
being that we didn't want to break compatibility of such an often used
API after the Beta release.
