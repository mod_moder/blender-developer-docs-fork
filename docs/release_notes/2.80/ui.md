# Blender 2.80: User Interface

Blender has a revamped user interface, to make it easier to discover and
use its many features.

## Shortcut Keys

As Blender's functionality has expanded over the years, the number of
available shortcut keys for new features and users to map their own has
become too small. For that reason a number of shortcut keys were changed
to make often used functionality accessible quickly, while assigning
fewer shortcut keys by default so users can map their own.

The following keymaps are available:

- Blender: default keymap, designed for efficient work in Blender ([list
  of modified shortcuts](https://developer.blender.org/T55194))
- Blender 2.7: keymap with as few changes as possible compared to
  earlier versions
- Industry Compatible: keymap based on typical shortcuts in other 3D
  apps ([overview of shortcuts](https://developer.blender.org/T54963))

## Left Click Select

Blender now uses the left mouse button for selection by default. The
keymap for left click select has been re-done in a way that can make it
useable across all Blender features, including the new tools in the
toolbar. The updated left click select also works with Emulate 3 Button
Mouse, which a lot of laptop users use. You can now fully use Blender
with a one-button trackpad or with pen input.

It works as follows:

- Left click to select (hold shift or ctrl to extend or remove
  selection).
- Right click to open a context menu.
- With the default Box Select tool, drag to make a box selection, and
  use Shift or Ctrl to extend or remove.
- Shift-RMB to set the 3D Cursor from any tool, or switch to the 3D
  Cursor tool and simply click.
- W key always returns to the Box Select tool.

Note that we will continue to support right click select, for its
efficiency and health advantages when using a keyboard and mouse.

## New Theme and Icons

Blender has a redesigned look and feel with a new dark theme and modern
icon set, designed to let you focus on your artwork rather than
distracting UI elements. The icons are now coloured by the theme, so
that we can always ensure that they are readable against bright or dark
backgrounds.

For users that prefer a light user interface theme or other variation,
various theme presets are available when setting up Blender for the
first time, and in the user settings.

## Context and Quick Favorites Menus

Throughout Blender, there is now a right-click context menu, which
provides quick access to important commands in the given context.

Additional, a new Quick Favorites menu (Q key) is available to gather
your favorite commands. By right-clicking on buttons and menus, any
command or menu can be added to this popup menu.

## Templates

When starting a new file, there is now a choice between multiple
application templates:

- General: 3D modeling, shading, animation and rendering.
- 2D Animation: grease pencil based 2D drawing and animation.
- Sculpting: simple setup to get started with sculpting.
- VFX: motion tracking, masking and compositing for visual effects.
- Video Editing: for using Blender as a video editor.

For each template a different startup file can be saved.

## Workspaces

Workspaces provide a screen layout for specific tasks like modeling,
sculpting or texture painting. They are available as tabs at the top of
the window.

Each template provides a default set of workspaces which can be
customized. New workspaces can be created, or copied from one of the
templates. It is also possible to append workspaces from any .blend file
with File \> Append. Workspaces can be deleted by right clicking on
them.

Multiple workspaces can be opened at the same time, with Window \> New
Main Window. Then each window can be set to its own workspace, scene and
view layer. Each workspace can also have multiple child windows with
Window \> New Window and View Area \> Duplicate Area into New Window in
each editor.

Each workspace has an associated object mode, that is automatically
activating when switching to that workspace.

## Tools and Tool Settings

The 3D viewport has a new toolbar on the left. The new tools in the
toolbar are different from existing commands, in that they are activated
and stay active until changing to another tool.

Their settings are displayed in a bar at the top of the Window. For
tools with more detailed settings (like sculpt brushes), the same tool
settings are also available in the Tool tab in the Properties editor.

Some addons will be available as new tools in the toolbar. Others will
be moved to tabs and panels in the sidebar on the right of the 3D
viewport.

## Adjust Last Operation

For tweaking the settings of the previously applied operation, editors
now show a panel in the bottom left corner.

These are different than the tool settings in the top bar and properties
editor, which control what the next operation does.

## Status Bar

At the bottom of windows there is now a status bar. This bar shows:

- Information about what mouse buttons and keys do for each editor, mode
  and tool, and it updates as you hold modifier keys.
- Messages and progress indicators for tools and jobs.
- Scene statistics.

The statusbar can be optionally be hidden by dragging it down or from
the Window menu.

## Properties Editor

The properties editor was reorganized to have buttons mainly a single
column layout. Panels can also have [
subpanels](python_api/ui_api.md#sub-panels)
now, which more advanced and less commonly used settings collapsed by
default.

Presets have been moved into the panel headers to save space.

Next to each property, we now have 'decorators', little indicators that
describe the current state of each property, so you can see if it is
animated, a keyframe, is driven, linked or overridden.

Properties that point to objects and bones (e.g. constraint targets) now
have a new 'Jump To Target' right click menu option that selects the
target object/bone.

## Preferences

The Preferences window has been re-designed with a vertical source list,
and a more organized Properties-like layout using panels and
subsections. Many related options, which were previously scattered, have
been grouped together in panels. The Theme section in particular is now
more structured and flexible, making use of the adaptive layout system.

## Popovers

In 2.8 we now have the concept of popovers, which allows us essentially
add a panel that spawns as a menu. This allows us to group related
features together in a way that can stay out of the way when you are not
using them.

For efficient access to single options, hold down the mouse button when
clicking on the popover and release it over the option you want to
change.

## Pie Menus

We now include a set of pie menus by default, to make more efficient use
of the keyboard to group related features. Pie menus are fast because
you can use them via a gesture. Hold the key, swipe in a direction, and
release. Over time, users can gain muscle memory in order to quickly
repeat actions.

In the default keymap, pie menus are used for Orientation (,), Pivot
(.), viewport (~), shading (z), Snap (Shift-S) and Proportional Editing
Falloff (Shift-O)
