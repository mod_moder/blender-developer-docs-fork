# Blender 3.0: Python API

## Bundled Python

- `zstandard` package was added to bundled python to enable scripts to
  manipulate zstd-compressed .blend files
  (blender/blender@a5917175,
  see also the [.blend file read/write release
  notes](core.md#blend-file-read-.26-write)).

## Text Editor

- The "Register" option to run Python scripts on startup no longer
  requires a `.py` extension in the name
  (blender/blender@3e775a4fc57cfd48954adcf284354312f34d5412).

## Other Additions

- Added multi-dimensional array support for `bpy.props` vector types
  (`FloatVectorProperty`, `IntVectorProperty`,
  `BoolVectorProperty`).
  (blender/blender@bc0a7d3fae5cfbe76ff84b76cb0ce48fe46adea5).

  
Example that defines a 4x4 matrix property:

``` python
bpy.props.FloatVectorProperty(size=(4, 4), subtype='MATRIX')
```

- Sequencer: Add Sequence.parent_meta() python API function
  (blender/blender@eec1ea0ccf2)
- In a File Browser, a list of selected files can be queried via
  `bpy.context.selected_files`
  (blender/blender@7ec839adfa99).
- In a File Browser, `FileSelectEntry.relative_path` gives a file's
  path (including the file name) relative to the currently displayed
  directory. This becomes relevant when the File Browser is set to show
  multiple levels of recursion. Multiple files with the same name may be
  visible then. So do not use the file name to reference file, use
  `FileSelectEntry.relative_path` instead.
  (blender/blender@79281336c055)
- `mathutils.{Vector/Matrix/Euler/Color/Quaternion}` now have an
  `is_valid` attribute which can be used to check if their owner is
  still valid
  (blender/blender@0950cfd9d53ef666cf820765e83586f72b04e9b6).
- Area maintenance "Close" can now be scripted, eg:
  `bpy.ops.screen.area_close({"area":
  bpy.context.screen.areas\[0\]})`.
  (blender/blender@9290b41381fd)
- User Interface: expose additional mouse cursors \[`PICK_AREA`, `STOP`,
  `COPY`, `CROSS`, `MUTE`, `ZOOM_IN`, `ZOOM_OUT`\]
  (blender/blender@6bf8c95e521d6effe9e1c426e14efe20dac81175).
- User Interface: new operator setting for `bl_options` called
  `DEPENDS_ON_CURSOR` to be used so operators wait for input when
  accessed from menus
  (blender/blender@da2ba402689d7ed752b0b37ad49dce1a5851cc77).
- User Interface: support setting the cursor when `DEPENDS_ON_CURSOR` is used via
  `OperatorType.bl_cursor_pending`
  (blender/blender@2a8e5128c16c17a7b2f6fc5325dc8f5abb4427d4).

## Other Changes

- Added functions to ensure and clear an RNA struct's IDProperties: `
  id_properties_clear()` and `id_properties_ensure()`
  (blender/blender@c202d3865904).
- Added `Operator.poll_message_set(message, ...)`  
  Operators can now set a message for why an operators poll function
  fails
  (blender/blender@ebe04bd3cafaa1f88bd51eee5b3e7bef38ae69bc).
- New `bpy.app.handlers.xr_session_start_pre` handler that executes
  when starting a virtual reality session. Main use-case is to pass a VR
  controller action map (the XR equivalent of a key-map) to the session
  (blender/blender@cb12fb78cad4).
- New `XrSessionSettings.use_absolute_tracking` property that, when
  `true`, allows the VR tracking origin to be defined independently of
  the headset position
  (blender/blender@36c0649d32aa).
- New `XrSessionSettings.base_scale` property that defines the VR
  viewer's reference scale
  (blender/blender@3434a991ec5b).
- New `XrSessionSettings` properties for VR overlays
  (`show_selection`, `show_controllers`, `show_custom_overlays`)
  and controller visualization (`controller_draw_style`)
  (blender/blender@9dda65455b54).
- New `XrSessionState` methods for VR actions
  (blender/blender@e844e9e8f3bb).
  - Action creation:`action_set_create()`, `action_create()`,
    `action_binding_create()`, `active_action_set_set()`,
    `controller_pose_actions_set()`
  - Action querying:`action_state_get()`
  - Haptics:`haptic_action_apply()`,`haptic_action_stop()`
  - Controller poses:`controller_grip_location_get()`,
    `controller_grip_rotation_get()`,
    `controller_aim_location_get()`, `controller_aim_rotation_get()`
- New `XrSessionState.actionmaps` collection property to store default
  VR action maps. Main use-case is to load default action maps from a
  Python script into this collection and then create actions for the
  session during the `xr_session_start_pre` handler
  (blender/blender@e844e9e8f3bb).
- New `XrSessionState.navigation_location/rotation/scale` properties
  that allow the VR viewer to navigate the viewport by adding these
  deltas to the viewer's base pose/scale
  (blender/blender@3434a991ec5b).
- New `XR_ACTION` event type and `Event.xr` property for accessing
  XR event data such as input states and controller poses
  (blender/blender@cdeb506008e9).
- New `XR` region type for drawing to the VR viewport (headset
  display) and mirror region. Add-ons can use
  `Space.draw_handler_add()` with region type `XR` and draw type
  `POST_VIEW` for VR custom drawing
  (blender/blender@9dda65455b54).
- New `Matrix.LocRotScale` constructor for combining all
  transformation channels into a matrix in one call.
  (blender/blender@a86e815dd86).

## Breaking Changes

- Replaced `PoseBone.custom_shape_scale` scalar with a
  `PoseBone.custom_shape_scale_xyz` vector; in addition,
  `custom_shape_translation` and `custom_shape_rotation_euler` were
  added.
  (blender/blender@fc5bf09fd88c)
- Renamed `bbone_curveiny`/`bbone_curveouty` to
  `bbone_curveinz`/`bbone_curveoutz`. Combined scale channels into
  `bbone_scalein.x`/`y`/`z` and `bbone_scaleout.x`/`y`/`z`
  vectors, with original `y` also becoming `z` to make space for an
  actual Y scale channel.
  (blender/blender@682a74e0909)
- Remove unused `BMesh.from_object` deform argument
  (blender/blender@ead084b6e1a05b9bff943660ea104efbed9b845f).
- Remove `context.active_base`
  (blender/blender@44db4e50b245041ea250bef4e525e658e83403cb).
- Remove use of `wiki_url` for add-ons `bl_info` (use "doc_url"
  instead)
  (blender/blender@aaa07a3a8f6427a01ef1ffacc0733e82921b0a8a).
- Remove `IDPropertyGroup.iteritems`, now `keys`, `values` &
  `items` all use iterators
  (blender/blender@265d97556aa0f0f2a0e4dd7584e3b8573bbddd54).
- Some RNA accessors were **creating** data, which is very bad on design
  level for several reasons, the following were changed:
  - Force fields and collision settings (`Object.field`,
    `Object.collision`, `ParticleSettings.force_field_1` and
    `ParticleSettings.force_field_2`). Object ones can be `None` and
    need to be added explicitly now (`Collision` modifier and
    `forcefield_toggle` operator), particle ones are always generated
    together with particles settings.
  - ID preview can be `None`, can use the new `preview_ensure`
    function first then.
- Remove `bpy.app.binary_path_python`
  (blender/blender@90b0fb135fdd06055d42f22396120e812440122d)
  (use `sys.executable` instead)
- Replace `UI_EMBOSS_NONE_OR_STATUS` with `NONE_OR_STATUS` in
  `UILayout.emboss`
  (blender/blender@0afe4e81cbb2).
- Remove non-functional `renderable_only` arg from
  `bpy.ops.wm.alembic_export`
  (blender/blender@834e87af7bbf).
- Move `preferences.view.show_layout_ui` to
  `preferences.apps.show_corner_split`
  (blender/blender@9fee59a4849).
- Rename `bpy.types.TOPBAR_MT_app` and
  `bpy.types.TOPBAR_MT_app_system` to `bpy.types.TOPBAR_MT_blender`
  and `bpy.types.TOPBAR_MT_blender_system`
  (blender/blender@58043c0637f8).
- Remove icon `SMALL_TRI_RIGHT_VEC` (use `DISCLOSURE_TRI_RIGHT` for
  a similar icon).
  (blender/blender@368d7944073e).

### IDProperty UI Data API

`IDProperty` UI data storage was refactored to remove the special
`_RNA_UI` subgroup, adding a proper manager for a property's UI data
(blender/blender@8b9a3b94fc148d).

![IDProperty UI data before 3.0. UI data is stored in the `_RNA_UI` subgroup.](../../images/IDProperty_UI_Data_Before_3.0.png)

![UI data is stored directly in the property now.](../../images/IDProperty_UI_data_after_3.0.png)

- Python objects with custom properties now have an
  `id_properties_ui(prop_name)` method, which returns a manager for
  the UI data, with the following methods:
  - `update`: Update UI data values, like `min=1.0` or
    `default="new_default"`
  - `as_dict`: Returns a dictionary containing the UI data values
  - `clear`: Removes the property's UI data.
  - `update_from`: Copy UI data between properties, even if they have
    different owners.
- **Note**: code that simply wants to create a property with UI data is
  advised to switch to the `rna_idprop_ui_create` utility function
  from the `rna_prop_ui` module, which has existed since 2.80 and was
  transparently updated to preserve compatibility through this change to
  the internals.

**Supported UI Data**

| Property Type | Min | Max | Soft Min | Soft Min | Step | Precision | Default        | Subtype | Description |
|---------------|-----|-----|----------|----------|------|-----------|----------------|---------|-------------|
| Float         | X   | X   | X        | X        | X    | X         | X (and arrays) | X       | X           |
| Integer       | X   | X   | X        | X        | X    | X         | X (and arrays) | X       | X           |
| String        |     |     |          |          |      |           | X              | X       | X           |
| ID            |     |     |          |          |      |           |                | X       | X           |

### Geometry Instancing

Previously, objects were the smallest unit of instancing. Now,
object-data, such as meshes, can be instanced without creating an object
with that data first.
(blender/blender@5a9a16334c573c)

The API for accessing instances did not change, one just has to be more
careful now.

``` python
import bpy
depsgraph = bpy.context.view_layer.depsgraph
for object_instance in depsgraph.object_instances:
    if object_instance.is_instance:
        print(object_instance.object.data, object_instance.instance_object.data)
```

Previously, `.object.data` and `.instance_object.data` were always
the same. Now they can be different and it is correct to use
`.object.data`.

- `object_instance.object`: A temporary object wrapping the instance.
  References to this object must not be stored between loop iterations.
- `object_instance.object.data`: Actual object-data/geometry of the
  instance. This data should be rendered/exported. It can be stored
  between loop iterations and is still valid after the loop.
- `object_instance.matrix_world`: Transformation matrix of the
  instance.
- `object_instance.instance_object`: Object that created and owns the
  instance.
- `object_instance.instance_object.data`: Data of the object that
  created the instance. This should probably never be used now.

### Use Keyword Only Arguments

- `addon_utils.module_bl_info` 2nd arg `info_basis`.
- `addon_utils.modules` 1st `module_cache`, 2nd arg `refresh`.
- `addon_utils.modules_refresh` 1st arg `module_cache`.
- `bl_app_template_utils.activate` 1nd arg `template_id`.
- `bl_app_template_utils.import_from_id` 2nd arg `ignore_not_found`.
- `bl_app_template_utils.import_from_path` 2nd arg `ignore_not_found`.
- `bl_keymap_utils.keymap_from_toolbar.generate` 2nd & 3rd args `use_fallback_keys` & `use_reset`.
- `bl_keymap_utils.platform_helpers.keyconfig_data_oskey_from_ctrl 2nd arg `filter_fn`.
- `bl_ui_utils.bug_report_url.url_prefill_from_blender 1st arg `addon_info`.
- `bmesh.types.BMFace.copy` 1st & 2nd args `verts`, `edges`.
- `bmesh.types.BMesh.calc_volume` 1st arg `signed`.
- `bmesh.types.BMesh.from_mesh` 2nd..4th args `face_normals`, `use_shape_key`, `shape_key_index`.
- `bmesh.types.BMesh.from_object` 3rd & 4th args `cage`, `face_normals`.
- `bmesh.types.BMesh.transform` 2nd arg `filter`.
- `bmesh.types.BMesh.update_edit_mesh` 2nd & 3rd args `loop_triangles`, `destructive`.
- `bmesh.types.{BMVertSeq,BMEdgeSeq,BMFaceSeq}.sort` 1st & 2nd arg `key`, `reverse`.
- `bmesh.utils.face_split` 4th..6th args `coords`, `use_exist`, `example`.
- `bpy.data.libraries.load` 2nd..4th args `link`, `relative`, `assets_only`.
- `bpy.data.user_map` 1st..3rd args `subset`, `key_types`, `value_types`.
- `bpy.msgbus.subscribe_rna` 5th arg `options`.
- `bpy.path.abspath` 2nd & 3rd args `start` & `library`.
- `bpy.path.clean_name` 2nd arg `replace`.
- `bpy.path.ensure_ext` 3rd arg `case_sensitive`.
- `bpy.path.module_names` 2nd arg `recursive`.
- `bpy.path.relpath` 2nd arg `start`.
- `bpy.types.EditBone.transform` 2nd & 3rd arg `scale`, `roll`.
- `bpy.types.Operator.as_keywords` 1st arg `ignore`.
- `bpy.types.Struct.{keyframe_insert,keyframe_delete}` 2nd..5th args `index`, `frame`, `group`, `options`.
- `bpy.types.WindowManager.popup_menu` 2nd & 3rd arg `title`, `icon`.
- `bpy.types.WindowManager.popup_menu_pie` 3rd & 4th arg `title`, `icon`.
- `bpy.utils.app_template_paths` 1st arg `subdir`.
- `bpy.utils.app_template_paths` 1st arg `subdir`.
- `bpy.utils.blend_paths` 1st..3rd args `absolute`, `packed`, `local`.
- `bpy.utils.execfile` 2nd arg `mod`.
- `bpy.utils.keyconfig_set` 2nd arg `report`.
- `bpy.utils.load_scripts` 1st & 2nd `reload_scripts` & `refresh_scripts`.
- `bpy.utils.preset_find` 3rd & 4th args `display_name`, `ext`.
- `bpy.utils.resource_path` 2nd & 3rd arg `major`, `minor`.
- `bpy.utils.script_paths` 1st..4th args `subdir`, `user_pref`, `check_all`, `use_user`.
- `bpy.utils.smpte_from_frame` 2nd & 3rd args `fps`, `fps_base`.
- `bpy.utils.smpte_from_seconds` 2nd & 3rd args `fps`, `fps_base`.
- `bpy.utils.system_resource` 2nd arg `subdir`.
- `bpy.utils.time_from_frame` 2nd & 3rd args `fps`, `fps_base`.
- `bpy.utils.time_to_frame` 2nd & 3rd args `fps`, `fps_base`.
- `bpy.utils.units.to_string` 4th..6th `precision`, `split_unit`, `compatible_unit`.
- `bpy.utils.units.to_value` 4th arg `str_ref_unit`.
- `bpy.utils.user_resource` 2nd & 3rd args `subdir`, `create`
- `bpy_extras.view3d_utils.location_3d_to_region_2d` 4th arg `default`.
- `bpy_extras.view3d_utils.region_2d_to_origin_3d` 4th arg `clamp`.
- `gpu.offscreen.unbind` 1st arg `restore`.
- `gpu_extras.batch.batch_for_shader` 4th arg `indices`.
- `gpu_extras.batch.presets.draw_circle_2d` 4th arg `segments`.
- `gpu_extras.presets.draw_circle_2d` 4th arg `segments`.
- `imbuf.types.ImBuf.resize` 2nd arg `resize`.
- `imbuf.write` 2nd arg `filepath`.
- `mathutils.kdtree.KDTree.find` 2nd arg `filter`.
- `nodeitems_utils.NodeCategory` 3rd & 4th arg `descriptions`, `items`.
- `nodeitems_utils.NodeItem` 2nd..4th args `label`, `settings`, `poll`.
- `nodeitems_utils.NodeItemCustom` 1st & 2nd arg `poll`, `draw`.
- `rna_prop_ui.draw` 5th arg `use_edit`.
- `rna_prop_ui.rna_idprop_ui_get` 2nd arg `create`.
- `rna_prop_ui.rna_idprop_ui_prop_clear` 3rd arg `remove`.
- `rna_prop_ui.rna_idprop_ui_prop_get` 3rd arg `create`.
- `rna_xml.xml2rna` 2nd arg `root_rna`.
- `rna_xml.xml_file_write` 4th arg `skip_typemap`.

See commit
(blender/blender@f29a738e23ce487fc9ae759326fc3409b82f8b26).

### Motion Blur Velocities

Fluid and Alembic modifier properties for vertex velocities were
removed. These were used for motion blur on meshes with changing
topology, where vertex positions can't be matched with other frames.
(blender/blender@128eb6c)

- `FluidDomainSettings.mesh_vertices`
- `MeshSequenceCacheModifier.vertex_velocities`,
  `MeshSequenceCacheModifier.has_velocity`,
  `MeshSequenceCacheModifier.read_velocity`

Instead, renderers and exporters should use the `velocity` attribute
on meshes.

``` python
for attribute in mesh.attributes:
    if attribute.name == 'velocity' and attribute.data_type == 'FLOAT_VECTOR':
         print([item.vector for item in attribute.data])
```

The unit of velocity also changed, it is now Blender scene distance
units per second. Previously vertex velocities were relative to frames
instead of seconds.
