# Blender 4.1: Rendering

## Lights

Point and Spot lights have a new Soft Falloff option, that makes the lights render the same as in Blender 3.6 and earlier. This behavior is not physically based, but helps avoid sharp boundaries when the light intersects with other geometry. The option is enabled by default on new lights. (blender/blender!117832)

## Shader Nodes

### Musgrave Texture

The Musgrave Texture node was replaced by the Noise Texture node.
Existing shader node setups are converted automatically, and the
resulting renders are identical.
(blender/blender@0b11c591ec62d4ff405108c453f4a34bf3eaee91)

- The Dimension input is replaced by a Roughness input, where
  `Roughness = power(Lacunarity, -Dimension)`.
- The Detail input value must be subtracted by 1 compared to the old
  Musgrave Texture node.

By being part of the Noise Texture node, the Musgrave modes gain support
for the Distortion input and Color output.

![Node setup to convert Dimension to new Roughness](../../images/Cycles4.1_musgrave_roughness_conversion.png)

## Hydra

- Support rendering particle system hair, in addition to the existing
  geometry node hair curves (blender/blender@9a7ea993d08f672d780e2b6c64ac000cdb3c149f).
- Improved support for shader conversion to MaterialX, including math node
  (blender/blender@e5b7768830745071fae98f1a2fe3ffc137c6e5f7).
- Exports of large meshes has been parallelized, giving
  up to a 6x speedup depending on the scene and hardware
  (blender/blender@75f24a5f144751fdf2a13b8eb881d23846111fef).
