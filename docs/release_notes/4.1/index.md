# Blender 4.1 Release Notes

Blender 4.1 is currently in **Beta**. Phase **Bcon3** until March 13,
2024.
[See schedule](https://projects.blender.org/blender/blender/milestone/18).

Under development in [`blender-v4.1-release`](https://projects.blender.org/blender/blender/src/branch/blender-v4.1-release).

* [Animation & Rigging](animation_rigging.md)
* [Compositor & Sequencer](vfx.md)
* [EEVEE & Viewport](eevee.md)
* [Geometry Nodes](nodes_physics.md)
* [Modeling](modeling.md)
* [Sculpting](sculpt.md)
* [Import & Export](pipeline_assets_io.md)
* [User Interface](user_interface.md)
* [Rendering](rendering.md)
* [Cycles](cycles.md)
* [Add-ons](add_ons.md)
* [Python API](python_api.md)

## Compatibility

Libraries have been upgraded to match [VFX platform 2024](https://vfxplatform.com/), including:

* Python 3.11
* OpenColorIO 2.3
* OpenEXR 3.2
* OpenVDB 11.0
* OpenShadingLanguage 1.13
* USD 23.11

macOS 11.2 (Big Sur) is now the minimum required version for Apple computers.