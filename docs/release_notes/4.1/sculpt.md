# Blender 4.1: Sculpting

- Voxel remesh attribute preservation has been changed to propagate
  *all* attributes and to improve performance
  (blender/blender@ba1c8fe6a53a00c9324607db6a1836132cef5352).

- Add brush settings for view and normal automasking values
  (blender/blender@1e4f133950d48ce3c41e68ed8431b89176ea9433).
