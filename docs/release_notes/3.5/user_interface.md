# User Interface

## General

- The icon preview shown in the material slots and material search menu
  will no longer be rendered for linked in materials. This would cause
  slowdowns and UI freezes every time the file is reloaded, since the
  preview is never stored in files.
  (blender/blender@571f373155cb)
- More menus align their item labels when some items include an icon and
  some don't
  (blender/blender@58752ad93c).
- Font previews now differentiate better between Korean, Japanese,
  Simplified & Traditional Chinese
  (blender/blender@485ab420757b)

![](../../images/FontPreviewsCJK.png){style="width:800px;"}

## Viewport

- Cancelling viewport orbit/pan/zoom & dolly is now supported (by
  pressing RMB), this allows orbiting out of camera or orthographic
  views temporarily.
  (blender/blender@0b85d6a030a3fc5be10cce9b5df7a542a78b4503)

## macOS

- User notifications for finishing renders and compiling shaders were
  removed.
  (blender/blender@3590e263e09d)
