# Blender 3.5 Release Notes

Blender 3.5 was released on March 29, 2023.

Check out the [release notes on
blender.org](https://www.blender.org/download/releases/3-5/).

## [Animation & Rigging](animation_rigging.md)

## [Core](core.md)

## [EEVEE & Viewport](eevee.md)

## [Grease Pencil](grease_pencil.md)

## [Modeling](modeling.md)

## [Nodes & Physics](nodes_physics.md)

## [Pipeline, Assets & I/O](pipeline_assets_io.md)

## [Python API](python_api.md)

## [Cycles](cycles.md)

## [Sculpt, Paint, Texture](sculpt.md)

## [User Interface](user_interface.md)

## [VFX & Video](vfx.md)

## [Add-ons](add_ons.md)

## Compatibility

Blender 3.5 is compatible with [VFX Reference Platform
2023](https://vfxplatform.com/)

New minimum requirements:

- **macOS**
  - **macOS 10.15** (Catalina) for **Intel** devices
  - macOS 11.0 (Big Sur) for Apple Silicon (same requirements as
    previous releases)
- **Linux** distribution with **glibc 2.28**, including:
  - **Ubuntu 18.10**
  - **Debian 10** (Buster)
  - **Fedora 29**
  - **RHEL 8** and derivatives CentOS, Rocky Linux and AlmaLinux

## [Corrective Releases](corrective_releases.md)
