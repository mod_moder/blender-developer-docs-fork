- Image: Flip image operator
  blender/blender@7d874b03433.
- Sculpt: Expand Operator
  blender/blender@82e70324772.
- Sculpt: Mask Init operator
  blender/blender@74052a9f02a.
- Sculpt: Init Face Sets by Face Sets boundaries
  blender/blender@e5c1e13ef09.
- The Transfer Mode operator allows to switch objects while in Sculpt
  Mode by pressing the D key
  blender/blender@86915d04ee38
