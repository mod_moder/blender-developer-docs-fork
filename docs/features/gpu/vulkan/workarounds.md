# Workarounds


## 24 bit depth support

Not all vulkan platforms support 24 bit depth textures. They prefer to use 32F depth
textures. Blender still rely on using 24 bit depth textures, but should be phased out
after we fully migrate to Vulkan.


## RGB Texture support

`GPU_RGB16F` isn't support by almost all platforms. This texture format is in used
by color management, external render engines and the real-time compositor. If a
texture is created this format it is automatically converted to `GPU_RGBA16F`.

`GPU_RGB32F` isn't support by almost all platforms. This texture format is in used
by color management, external render engines and the real-time compositor. If a
texture is created this format it is automatically converted to `GPU_RGBA32F`.


## Data formats without benefits

Vulkan doesn't support texture formats that don't have any benefit performance wise.
For example using a 32 bit integer and using it as a float requires conversion, but
doesn't have any bandwidth improvements. Therefor in the core of vulkan it isn't allowed
to use these configurations. As workaround we convert the 32 bit interger to float
when uploading the data to the GPU.

Some platforms don't allow to using 3 component byte data in a vertex buffer. Internally
those formats are 32 bits in the vertex buffer. The workaround is to use 4 components
and fill the last component with the value 255.


## Depth Range

The default OpenGL depth range is between -1 and 1. Before entering the fragment stage
the depth value is normalized. Vulkan has a depth range of 0 to 1. Depth values between
-1 and 0 are clipped away. To workaround this issue the `gl_Position.z` is retargetted
to 0 to 1.

The main difference is that precision is lost as the order of clipping and normalization
is swapped. The same solution is done in the Metal backend.


## Framebuffer flipping

OpenGL uses a different coordinate system then Vulkan (and Metal) We need to flip the Y
coordinates to convert to the same coordinate system. For Vulkan we flip the coordinate
system when blitting/drawing the editors to the swap chain image.

> NOTE: Metal uses a different approach where all Y coordiates are flipped by the vertex stage.