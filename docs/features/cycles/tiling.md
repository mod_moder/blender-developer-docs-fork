# Cycles Tiling and OpenEXR Cache

## Overview

When rendering high resolution images with many render passes, memory
usage of the render buffer can become problematic. For this reason there
is a mechanism to render the image in tiles, and write each tile to an
OpenEXR cache file on disk as it is finished. Only at the end of
rendering do we read back the full render buffer for all view layer, and
perform denoising over the full resolution buffer if needed.

Note that this tiling mechanism is different than the tiling used for
GPU scheduling. An OpenEXR tile is relatively large, but not too large
to fit in device memory at once. A GPU scheduling tile can be small and
is used for incrementally scheduling more paths to be rendered to keep
the GPU occupied. Small tiles in GPU scheduling can also help improve
coherence, by rendering nearby pixels on the same multiprocessor.

## OpenEXR Cache File

TODO

## Pause / Resume

TODO

## Denoising

TODO

## Overlap

TODO
