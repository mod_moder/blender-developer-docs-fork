# Cycles Units & Colors

## Units

All **object**, **mesh** and **camera** distance units are in meters.

The values written out into the render float buffer are measured in
radiance.

    radiance = {Watts \over sr \cdot m ^ 2}

**Background** closures output radiance as well.

**Emission** closures may output different things depending on where
they are used. Surfaces by default output radiant exitance, but have an
option to emit radiant power over the entire surface area.

    exitance = {Watts \over m^2}

    power = {Watts \over 1}

Point lights emit radiant power, and distant lights emit radiance.

The output of **BSDF** nodes is unitless, it is a probability of light
being reflected or transmitted.

## Color Spaces

All colors in the render **kernel** are referred to as scene linear.
This also means that we do not make a distinction between different
color spaces for e.g. bump maps and diffuse maps at that level. We
intend to keep color space conversions out of the render kernel.

Also **images textures** are assumed to be scene linear, or scene
linear + sRGB transfer function for compression into 8 bit. When we add
support for more color spaces later, we should do this on file read,
before mipmapping and interpolation to ensure proper energy
preservation.

The float buffer resulting from the render is also scene linear. For
**display** there is a conversion from this float buffer to an 8bit srgb
buffer suitable for computer screens.

### Spectral Rendering

We currently do not support spectral rendering, and only use RGB colors.
Part of the reason is that Open Shading Language also uses RGB. Even if
the shading system does not, integrators could still be made to use
spectral colors, which would at least make light interaction spectrally
correct.
