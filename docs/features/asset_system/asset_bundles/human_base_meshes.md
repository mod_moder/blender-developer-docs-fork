# Human Base Meshes Bundle

Base meshes to sculpt humanoid figures. The models are ready to use with
the Multiresolution modifier. But since they are all closed volumes they
can be voxel remeshed as well.

![](../../../images/Asset-base-humans.jpg){style="width:1024px;"}

## Models

- Base Mesh - Eye
- Base Mesh - Foot
- Base Mesh - Hand
- Base Mesh - Jaw
- Base Mesh - Stylized Female
- Base Mesh - Stylized Head
- Base Mesh - Stylized Male
